DROP TABLE IF EXISTS user_repository;

CREATE TABLE user_repository (
    id UUID PRIMARY KEY NOT NULL,
    name VARCHAR(250) NOT NULL,
    password VARCHAR(250) NOT NULL,
    email VARCHAR(250) NOT NULL,
    token UUID NOT NULL,
    active BOOLEAN NOT NULL,
    last_login TIMESTAMP NULL,
    created_on TIMESTAMP NOT NULL,
    modified_on TIMESTAMP NULL,

    UNIQUE KEY user_repository_uq_1 (email)
);

DROP TABLE IF EXISTS user_phone_detail;

CREATE TABLE user_phone_detail (
 id IDENTITY PRIMARY KEY NOT NULL,
 user_id UUID NOT NULL,
 number VARCHAR(8) NOT NULL,
 city_code VARCHAR(3) NOT NULL,
 country_code VARCHAR(3) NOT NULL,

 CONSTRAINT user_phone_detail_fk_1 FOREIGN KEY (user_id) REFERENCES user_repository(id)
);

INSERT INTO user_repository (id, name, password, email, token, active, last_login, created_on, modified_on) VALUES
    ('3aaef88b-2b55-4059-8c6c-7f61385ea956' /*id*/, 'Goku Rodríguez' /*name*/, '$2a$12$CYDpBjAEAQpGJNQ0KKZvOeldDshAbI7YUpfcN6NE0Gco44CRe0.52' /*password*/, 'goku.rodriguez@dragonball.com' /*email*/, 'de782d0b-96db-4e08-b17b-2ce2339fa253' /*token*/, 'true' /*active*/, '2021-09-25T06:26:29.260607' /*last_login*/, '2021-09-25T06:26:29.260607' /*created_on*/, null /*modified_on*/),
    ('88b1f9f0-06f6-4a8e-b4be-5e9f800d481f' /*id*/, 'Vegeta Real' /*name*/, '$2a$12$nBJZyBtkacoakIZkrOEceu8oY0Wx5H0UpORt061eLTvJ9CB3Haati' /*password*/, 'vegeta.real@dragonball.com' /*email*/, '1656b531-8ed3-45ae-9c6c-878e670c5806' /*token*/, 'true' /*active*/, '2021-09-25T06:34:18.240376' /*last_login*/, '2021-09-25T06:34:18.240376' /*created_on*/, null /*modified_on*/),
    ('12aee538-d1be-475c-b6e3-6a57337cfa10' /*id*/, 'Piccolo López' /*name*/, '$2a$12$tvfNglzX92CedjIvbTefAuo/AiAJIoRDK2kEVSfjrcXS0b9jMTFJO' /*password*/, 'piccolo.lopez@dragonball.com' /*email*/, 'ab51ddb5-16dd-473c-b014-39c0a7b50227' /*token*/, 'true' /*active*/, '2021-09-25T06:44:17.716389' /*last_login*/, '2021-09-25T06:44:17.716389' /*created_on*/, null /*modified_on*/);

INSERT INTO user_phone_detail (user_id, number, city_code, country_code) VALUES
    ('3aaef88b-2b55-4059-8c6c-7f61385ea956' /*user_id*/, '88802001' /*number*/, '1' /*city_code*/, '505' /*country_code*/),
    ('88b1f9f0-06f6-4a8e-b4be-5e9f800d481f' /*user_id*/, '88802011' /*number*/, '7' /*city_code*/, '506' /*country_code*/),
    ('12aee538-d1be-475c-b6e3-6a57337cfa10' /*user_id*/, '88802021' /*number*/, '9' /*city_code*/, '507' /*country_code*/);
