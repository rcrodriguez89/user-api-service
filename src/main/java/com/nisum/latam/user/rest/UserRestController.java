package com.nisum.latam.user.rest;

import com.nisum.latam.user.model.User;
import com.nisum.latam.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author rcrodriguez
 */
@RestController
@RequestMapping(
    value = "/users",
    produces = MediaType.APPLICATION_JSON_VALUE
)
public class UserRestController {

    @Autowired
    private UserService userService;

    @GetMapping
    public Response findAll() {
        return new Response(this.userService.findAll());
    }

    @GetMapping("/{id}")
    public User findById(@PathVariable String id) {
        return this.userService.findById(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> create(@RequestBody User newUser) {
        return new ResponseEntity<User>(this.userService.save(newUser), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public User update(@PathVariable String id, @RequestBody User userWithUpdatedData) {
        return this.userService.update(id, userWithUpdatedData);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
       this.userService.delete(id);
    }
}
