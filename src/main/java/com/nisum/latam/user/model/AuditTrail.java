package com.nisum.latam.user.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDateTime;

/**
 * @author rcrodriguez
 */
@Embeddable
@NoArgsConstructor
@Getter
@Setter
@ToString
public class AuditTrail {

    @NotNull
    @PastOrPresent
    @Column(name = "created_on", updatable = false)
    private LocalDateTime createdOn;

    @PastOrPresent
    @Column(name = "modified_on", insertable = false)
    private LocalDateTime modifiedOn;

    @PrePersist
    public void onPrePersist() {
        this.createdOn = LocalDateTime.now();
    }

    @PreUpdate
    public void onPreUpdate() {
        this.modifiedOn = LocalDateTime.now();
    }
}
