package com.nisum.latam.user.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author rcrodriguez
 */
@Entity
@Table(name = "user_phone_detail")
@Getter
@Setter
@NoArgsConstructor
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @NotEmpty
    @Column(name = "number")
    private String number;

    @NotNull
    @NotEmpty
    @Column(name = "city_code")
    private String cityCode;

    @NotNull
    @NotEmpty
    @Column(name = "country_code")
    private String countryCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @JsonValue
    public Map<String, Object> asMap() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("number", this.number);
        dto.put("citycode", this.cityCode);
        dto.put("country", this.countryCode);
        return dto;
    }
}
