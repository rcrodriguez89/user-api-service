package com.nisum.latam.user.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author rcrodriguez
 */
@Entity
@Table(name = "user_repository")
@NoArgsConstructor
@Getter
@Setter
public class User {
    @Id
    @Column(name = "id")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @NotEmpty
    @Column(name = "name")
    private String name;

    @NotNull
    @NotEmpty
    @Column(name = "password")
    private String password;

    @NotNull
    @NotEmpty
    @Email
    @Column(name = "email", unique = true)
    private String email;

    @NotNull
    @Column(name = "token")
    private UUID token;

    @Embedded
    private AuditTrail auditTrail = new AuditTrail();

    @NotNull
    @Column(name = "last_login")
    private LocalDateTime lastLogin;

    @Column(name = "active")
    private boolean active = true;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Phone> phones = new HashSet<>();

    @PrePersist
    public void onPrePersist() {
        this.id = UUID.randomUUID();
        this.token = UUID.randomUUID();
        this.lastLogin = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!getClass().equals(obj.getClass())) {
            return false;
        }

        User other = (User) obj;
        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void addPhone(Phone phone) {
        this.phones.add(phone);
    }

    public void removePhone(Phone phone) {
        this.phones.remove(phone);
    }

    public Optional<Phone> getPhone(Integer id) {
        Objects.requireNonNull(id);
        return this.phones.stream().filter(phone -> id.equals(phone.getId())).findFirst();
    }

    @JsonValue
    public Map<String, Object> asMap() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", this.id);
        dto.put("name", this.name);
        dto.put("created", this.auditTrail.getCreatedOn());
        dto.put("modified", this.auditTrail.getModifiedOn());
        dto.put("last_login", this.lastLogin);
        dto.put("token", this.token);
        dto.put("is_active", this.active);
        return dto;
    }
}
