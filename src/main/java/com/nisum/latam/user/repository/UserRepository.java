package com.nisum.latam.user.repository;

import com.nisum.latam.user.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * @author rcrodriguez
 */
@Repository
public interface UserRepository extends CrudRepository<User, UUID> {
}
