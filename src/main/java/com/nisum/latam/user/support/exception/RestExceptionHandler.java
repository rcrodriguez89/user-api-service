package com.nisum.latam.user.support.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.sql.SQLException;

/**
 * @author rcrodriguez
 */
@ControllerAdvice
@Slf4j
public class RestExceptionHandler {

    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ErrorMessageInfoResponse handleNotFoundException(NotFoundException ex) {
        return new ErrorMessageInfoResponse(ex);
    }

    @ExceptionHandler(SQLException.class)
    ResponseEntity<ErrorMessageInfoResponse> handleSQLException(SQLException ex) {
        log.error("SQL Error Code: {} | Info Error: {}", ex.getErrorCode(), ex.getMessage());
        return this.createResponseSQLInfoError(ex);
    }

    @ResponseBody
    @ExceptionHandler(HttpClientErrorException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ErrorMessageInfoResponse handleHttpClientErrorException(HttpClientErrorException ex) {
        log.error("Class: {} | Info Error: {}", ex.getClass().getName(), ex.getMessage());
        return new ErrorMessageInfoResponse("Internal Server Error. Please, try again");
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ErrorMessageInfoResponse handleGenericException(Exception ex) {
        log.error("Class: {} | Info Error: {}", ex.getClass().getName(), ex.getMessage());
        return new ErrorMessageInfoResponse("Internal Server Error. Please, try again");
    }

    private ResponseEntity<ErrorMessageInfoResponse> createResponseSQLInfoError(SQLException ex) {
        ErrorMessageInfoResponse errorMessage;

        if (ex.getMessage().indexOf("USER_REPOSITORY_UQ_1") >= 0 && ex.getErrorCode() == 23505) {

            errorMessage = new ErrorMessageInfoResponse("This email address already belongs to a registered user");
            return new ResponseEntity<ErrorMessageInfoResponse>(errorMessage, HttpStatus.BAD_REQUEST);
        } else {

            errorMessage = new ErrorMessageInfoResponse(String.format("SQL Error Code: %s | Info Error: %s", ex.getErrorCode(), ex.getMessage()));
            return new ResponseEntity<ErrorMessageInfoResponse>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
