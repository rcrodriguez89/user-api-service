package com.nisum.latam.user.support.exception;

/**
 * @author rcrodriguez
 */
public class NotFoundException extends RuntimeException {

    private static final String ENTITY_WITH_FIELD_NOT_FOUND_MESSAGE = "%s with %s [%s] not found";

    private static final String FIELD_ID = "ID";

    protected NotFoundException(String entity, Object id) {
        super(String.format(ENTITY_WITH_FIELD_NOT_FOUND_MESSAGE, entity, FIELD_ID, id));
    }

    protected NotFoundException(String entity, String field, Object value) {
        super(String.format(ENTITY_WITH_FIELD_NOT_FOUND_MESSAGE, entity, field, value));
    }
}
