package com.nisum.latam.user.support.exception;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author rcrodriguez
 */
public class ErrorMessageInfoResponse {

    private String message;
    private static final String NULL_OBJECT_MESSAGE = "Null Object";

    public ErrorMessageInfoResponse(final Exception exception) {
        this.message = exception == null ? NULL_OBJECT_MESSAGE : exception.getMessage();
    }

    public ErrorMessageInfoResponse(final String message) {
        this.message = message;
    }

    @JsonValue
    public Map<String, Object> asMap() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("message", this.message);
        return dto;
    }
}
