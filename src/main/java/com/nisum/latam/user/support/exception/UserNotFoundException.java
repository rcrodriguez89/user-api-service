package com.nisum.latam.user.support.exception;

/**
 * @author rcrodriguez
 */
public class UserNotFoundException extends NotFoundException {

    private static final String ENTITY_DESCRIPTION = "User";

    public UserNotFoundException(Object id) {
        super(ENTITY_DESCRIPTION, id);
    }
}
