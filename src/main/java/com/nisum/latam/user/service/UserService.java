package com.nisum.latam.user.service;

import com.nisum.latam.user.model.User;
import com.nisum.latam.user.repository.UserRepository;
import com.nisum.latam.user.support.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @author rcrodriguez
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Iterable findAll() {
        return userRepository.findAll();
    }

    public User findById(String id) {
        return this.userRepository.findById(UUID.fromString(id)).orElseThrow(() -> new UserNotFoundException(id));
    }

    public User save(final User newUser) {
        newUser.getPhones().stream()
            .forEach(phone -> {
                phone.setUser(newUser);
            });

        return this.userRepository.save(newUser);
    }

    public void delete(String id) {
        User userToDelete = this.userRepository.findById(UUID.fromString(id)).orElseThrow(() -> new UserNotFoundException(id));
        this.userRepository.delete(userToDelete);
    }

    public User update(String userId, User userWithUpdatedData) {
        User persistedUser = this.userRepository.findById(UUID.fromString(userId))
                .orElseThrow(() -> new UserNotFoundException(userId));

        persistedUser.setName(userWithUpdatedData.getName());
        persistedUser.setPassword(userWithUpdatedData.getPassword());
        persistedUser.setEmail(userWithUpdatedData.getEmail());
        persistedUser.setToken(userWithUpdatedData.getToken());
        persistedUser.setLastLogin(userWithUpdatedData.getLastLogin());
        persistedUser.setActive(userWithUpdatedData.isActive());
        persistedUser.setPhones(userWithUpdatedData.getPhones());

        return this.userRepository.save(persistedUser);
    }
}
